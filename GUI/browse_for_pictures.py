from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from PIL import Image, ImageTk
from functools import partial
import tkinter as tk
from Segmentator.main import ROOT_DIR
import os
import cv2

from Segmentator.main import main as main_script
from Segmentator.preview import main as preview_script
from Segmentator.linesegmenter import LineSegmenter

class Root(Tk):
    def __init__(self):
        super(Root, self).__init__()
        self.title("Text recognition")
        self.minsize(600, 400)

        self.labelFrame = ttk.LabelFrame(self, text="Open File")
        self.labelFrame.grid(column=0, row=1)

        self.word_segment_min_area_multiplier = 0.001
        self.char_segment_min_area_multiplier = 0.008

        self.word_segment_kernel_height = 15
        self.word_segment_kernel_width = 70

        self.char_segment_kernel_height = 5
        self.char_segment_kernel_width = 5

        self.line_segment_kernel_height = 2
        self.line_segment_kernel_width = 200
        """
        Label(self, text="Word segmenter - min area multiplier -> " + str(self.word_segment_min_area_multiplier),
              fg="black", font="none 8 bold").grid(row=2, column=0, sticky=W, padx=10)
        Label(self, text="Char segmenter - min area multiplier -> " + str(self.char_segment_min_area_multiplier),
              fg="black", font="none 8 bold").grid(row=3, column=0, sticky=W, padx=10)
        Label(self, text="Word segmenter - kernel height -> " + str(self.word_segment_kernel_height), fg="black",
              font="none 8 bold").grid(row=4, column=0, sticky=W, padx=10)
        Label(self, text="Word segmenter - kernel width -> " + str(self.word_segment_kernel_width), fg="black",
              font="none 8 bold").grid(row=5, column=0, sticky=W, padx=10)
        Label(self, text="Char segmenter - kernel height -> " + str(self.char_segment_kernel_height), fg="black",
              font="none 8 bold").grid(row=6, column=0, sticky=W, padx=10)
        Label(self, text="Char segmenter - kernel width -> " + str(self.char_segment_kernel_width), fg="black",
              font="none 8 bold").grid(row=7, column=0, sticky=W, padx=10)
        Label(self, text="Line segmenter - kernel height -> " + str(self.line_segment_kernel_height), fg="black",
              font="none 8 bold").grid(row=8, column=0, sticky=W, padx=10)
        Label(self, text="Line segmenter - kernel width -> " + str(self.line_segment_kernel_width), fg="black",
              font="none 8 bold").grid(row=9, column=0, sticky=W, padx=10)
        """
        self.ls_kernel_height = StringVar(self, value="2")
        self.ls_kernel_width = StringVar(self, value="200")
        self.ws_kernel_height = StringVar(self, value="15")
        self.ws_kernel_width = StringVar(self, value="70")
        self.cs_kernel_height = StringVar(self, value="5")
        self.cs_kernel_width = StringVar(self, value="5")
        self.ws_min_area = StringVar(self, value="0.001")
        self.cs_min_area = StringVar(self, value="0.008")

        self.button_browse()
        self.button_browse_model()
        self.button_convert()
        self.button_preview()

        # Label (self, text="Output: ", fg="black", font="none 12 bold") .grid(row=10, column=0, sticky=W, padx = 30)
        # writebox = Text(self, height=10, width=30, bg="white")
        # writebox.grid(row=11, column=0, sticky=W, padx = 50, pady = 50) #red ispisa

    def change_line_segmenter_values(self, kernelHeight, kernelWidth):
        num1 = (kernelHeight.get())
        num2 = (kernelWidth.get())
        self.line_segment_kernel_height = float(num1)
        self.line_segment_kernel_width = float(num2)
        self.preview()

    def change_word_segmenter_values(self, kernelHeight, kernelWidth, minArea):
        num1 = (kernelHeight.get())
        num2 = (kernelWidth.get())
        num3 = (minArea.get())
        self.word_segment_kernel_height = float(num1)
        self.word_segment_kernel_width = float(num2)
        self.word_segment_min_area_multiplier = float(num3)
        self.preview()

    def change_char_segmenter_values(self, kernelHeight, kernelWidth, minArea):
        num1 = (kernelHeight.get())
        num2 = (kernelWidth.get())
        num3 = (minArea.get())
        self.char_segment_kernel_height = float(num1)
        self.char_segment_kernel_width = float(num2)
        self.char_segment_min_area_multiplier = float(num3)
        self.preview()

    def button_browse(self):
        self.button = ttk.Button(self, text="Browse image", command=self.fileDialog)
        self.button.grid(column=0, row=2)

    def button_browse_model(self):
        self.button = ttk.Button(self, text="Browse model", command=self.modelDialog)
        self.button.grid(column=0, row=3)

    def button_convert(self):
        self.button = ttk.Button(self, text="Convert", command=self.convert)
        self.button.grid(column=0, row=4)

    def button_preview(self):
        self.button = ttk.Button(self, text="Preview", command=self.preview)
        self.button.grid(column=0, row=5)

    def fileDialog(self):

        self.file_path = filedialog.askopenfilename(initialdir="../image", title="Select a picture", filetype=
        (("Pictures", ".jpg .png .jpeg"), ("all files", "*.*")))
        self.label = ttk.Label(self, text="")
        self.label.grid(column=0, row=7)
        self.label.configure(text="Image: %s"%self.file_path)

        basewidth = 350
        img = Image.open(self.file_path)
        if img.mode in ("RGBA", "P"):
            img = img.convert("RGB")
        wpercent = (basewidth / float(img.size[0]))
        hsize = int((float(img.size[1]) * float(wpercent)))
        img = img.resize((basewidth, hsize), Image.ANTIALIAS)
        img.save('somepic.jpg')
        photo = ImageTk.PhotoImage(img)

        #  Label (self, text="Photo: ", fg="black", font="none 12 bold") .grid(row=1, column=0, sticky=E)
        #writebox = Text(self, height=10, width=30, bg="white")

        self.label2 = Label(image=photo)
        self.label2.image = photo
        self.label2.grid(column=0, row=0)  # red fotografije

    def modelDialog(self):
        self.model_file_path = filedialog.askdirectory(initialdir="../", title="Select model")
        self.label = ttk.Label(self, text="")
        self.label.grid(column=0, row=8)
        self.label.configure(text="Model: %s"%self.model_file_path)

    def show_preview(self):
        line_preview = Image.open("../GUI/preview/line_preview.png")
        if line_preview.mode in ("RGBA", "P"):
            line_preview = line_preview.convert("RGB")
        basewidth = 350
        wpercent = (basewidth / float(line_preview.size[0]))
        hsize = int((float(line_preview.size[1]) * float(wpercent)))
        line_preview = line_preview.resize((basewidth, hsize), Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(line_preview)
        self.label3 = Label(image=photo)
        self.label3.image = photo
        self.label3.grid(column=1, row=0, padx=10, pady=10)  # red fotografije
        Label(self, text="Kernel height:").grid(row=1, column=1, sticky=NW)
        Entry(self, textvariable=self.ls_kernel_height).grid(row=1, column=1, sticky=N)
        Label(self, text="Kernel width:").grid(row=2, column=1, sticky=NW)
        Entry(self, textvariable=self.ls_kernel_width).grid(row=2, column=1)
        change_line_segmenter_values = partial(self.change_line_segmenter_values, self.ls_kernel_height,
                                               self.ls_kernel_width)
        Button(self, text="Save", command=change_line_segmenter_values).grid(row=3, column=1)

        words_preview = Image.open("../GUI/preview/words_preview.png")
        if words_preview.mode in ("RGBA", "p"):
            words_preview = words_preview.convert("RGB")
        words_preview = words_preview.resize((basewidth, hsize), Image.ANTIALIAS)
        word_photo = ImageTk.PhotoImage(words_preview)
        self.label4 = Label(image=word_photo)
        self.label4.image = word_photo
        self.label4.grid(column=2, row=0, padx=10, pady=10)
        Label(self, text="Kernel height:").grid(row=1, column=2, sticky=NW)
        Entry(self, textvariable=self.ws_kernel_height).grid(row=1, column=2, sticky=N)
        Label(self, text="Kernel width:").grid(row=2, column=2, sticky=NW)
        Entry(self, textvariable=self.ws_kernel_width).grid(row=2, column=2, sticky=N)
        Label(self, text="Min area multiplier:").grid(row=3, column=2, sticky=NW)
        Entry(self, textvariable=self.ws_min_area).grid(row=3, column=2, sticky=N)
        change_word_segmenter_values = partial(self.change_word_segmenter_values, self.ws_kernel_height,
                                               self.ws_kernel_width, self.ws_min_area)
        Button(self, text="Save", command=change_word_segmenter_values).grid(row=4, column=2)

        char_preview = Image.open("../GUI/preview/characters_preview.png")
        if char_preview.mode in ("RGBA", "p"):
            char_preview = char_preview.convert("RGB")
        char_preview = char_preview.resize((basewidth, hsize), Image.ANTIALIAS)
        char_photo = ImageTk.PhotoImage(char_preview)
        self.label5 = Label(image=char_photo)
        self.label5.image = char_photo
        self.label5.grid(column=3, row=0, padx=10, pady=10)
        Label(self, text="Kernel height:").grid(row=1, column=3, sticky=NW)
        Entry(self, textvariable=self.cs_kernel_height).grid(row=1, column=3, sticky=N)
        Label(self, text="Kernel width:").grid(row=2, column=3, sticky=NW)
        Entry(self, textvariable=self.cs_kernel_width).grid(row=2, column=3, sticky=N)
        Label(self, text="Min area multiplier:").grid(row=3, column=3, sticky=NW)
        Entry(self, textvariable=self.cs_min_area).grid(row=3, column=3, sticky=N)
        change_char_segmenter_values = partial(self.change_char_segmenter_values, self.cs_kernel_height,
                                               self.cs_kernel_width, self.cs_min_area)
        Button(self, text="Save", command=change_char_segmenter_values).grid(row=4, column=3)

    def convert(self):
        text = main_script(
            self.file_path, self.model_file_path, self.line_segment_kernel_height, self.line_segment_kernel_width,
            self.word_segment_kernel_height,
            self.word_segment_kernel_width, self.word_segment_min_area_multiplier, self.char_segment_kernel_height,
            self.char_segment_kernel_width, self.char_segment_min_area_multiplier)

        file = open(("../model_output.txt"), "r")
        output = file.read()
        Label(self,text="Output:",font="none 12 bold").grid(row=5,column=1, sticky=N)
        blue = Text(root,height = 15, width = 50)
        blue.grid(row=6,column = 1)
        blue.insert(END,output)
        file.close()

    def preview(self):
        preview_script(
            self.file_path, self.line_segment_kernel_height, self.line_segment_kernel_width,
            self.word_segment_kernel_height,
            self.word_segment_kernel_width, self.word_segment_min_area_multiplier, self.char_segment_kernel_height,
            self.char_segment_kernel_width, self.char_segment_min_area_multiplier)

        self.label2=None
        self.label3=None
        self.label4=None
        self.label5=None

        self.show_preview()


root = Root()
root.mainloop()
