from tkinter import *

# main
window = Tk()
window.title("Text recognition AI")
window.configure(background="lightgray")
window.geometry("520x200")


#key down function
def click():
    output.delete(0.0, END)
    definition = "Vaš tekst"
    output.insert(END, definition)


#create label path
Label (window, text="Browse image path: ", bg="lightgray", fg="black", font="none 12 bold") .grid(row=1, column=0, sticky=W)

#path textbox path
pathbox = Entry(window, width=30, bg="white")
pathbox.grid(row=1, column=2, sticky=W)

#submit button
Button(window, text="Browse", width=6, command=click) .grid(row=1, column=3, sticky=W, padx=10, pady=10)

#create label output
Label (window, text="Output: ", bg="lightgray", fg="black", font="none 12 bold") .grid(row=4, column=0, sticky=W)

#path textbox output
textbox = Entry(window, width=40, bg="white")
textbox.grid(row=5, columnspan=6, sticky=W)

#execute button
Button(window, text="Convert to text", width=15, command=click) .grid(row=6, column=2, sticky=W, padx=10, pady=10)
