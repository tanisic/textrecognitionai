import os
import pickle
import random

import cv2
import numpy as np
from Segmentator import image_prepare as ip
import re

CATEGORIES = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
              "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]

training_data = []
IMG_SIZE = 32


def create_training_data(DATADIR):
    for category in CATEGORIES:
        if category.islower():
            DATADIR = "C:/Users/Tinac/Desktop/Train/lowercase"
        path = os.path.join(DATADIR, category)
        class_num = CATEGORIES.index(category)
        for img in os.listdir(path):
            if (re.findall(r"^_", img)):
                # os.remove(img)
                continue
            print("Adding: " + os.path.join(path, img))
            try:
                img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
                new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
                training_data.append([new_array, class_num])
            except Exception as e:
                pass
    print("Made " + str(len(training_data)) + " training data")


def create_and_prepare_our_dataset(DATADIR):
    for category in CATEGORIES:
        path = os.path.join(DATADIR, category)
        class_num = CATEGORIES.index(category)
        for img in os.listdir(path):
            if (re.findall(r"\B\(4\)", img)):
                os.remove(os.path.join(path, img))
                continue
            print("Adding: " + os.path.join(path, img))

            img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
            # gray = cv2.cvtColor(img_array, cv2.COLOR_BGR2GRAY)
            # blurred = cv2.GaussianBlur(img_array, (5, 5), 0)
            padded = ip.dilate(img_array, 0.02, 0.02, 2)
            thresh = cv2.threshold(padded, 127, 255, cv2.THRESH_BINARY_INV)[1]
            (tH, tW) = thresh.shape
            dX = int(max(0, 32 - tW) / 2.0)
            dY = int(max(0, 32 - tH) / 2.0)
            # pad the image and force 32x32 dimensions
            padded = cv2.copyMakeBorder(thresh, top=dY, bottom=dY, left=dX, right=dX,
                                        borderType=cv2.BORDER_CONSTANT, value=(0, 0, 0))
            new_array = cv2.resize(padded, (IMG_SIZE, IMG_SIZE))
            # cv2.imwrite(os.path.join(path, img),new_array)
            training_data.append([new_array, class_num])

    print("Made " + str(len(training_data)) + " training data")


create_training_data(DATADIR="C:\\Users\\Tinac\\Desktop\\lastest_newest")
# create_training_data(DATADIR="C:/Users/Tinac/Desktop/latest_dataset/")
random.shuffle(training_data)

X = []
y = []

for features, label in training_data:
    X.append(features)
    y.append(label)

X = np.array(X).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
y = np.asarray(y)
pickle_out = open("X.pickle", "wb")
pickle.dump(X, pickle_out)
pickle_out.close()

pickle_out = open("y.pickle", "wb")
pickle.dump(y, pickle_out)
pickle_out.close()
