import tensorflow as tf
import cv2
import os
import numpy as np
import matplotlib.pyplot as plt

CATEGORIES = [
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
    "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]


def prepare(filepath):
    IMG_SIZE = 32
    img_array = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
    #inverted_array = cv2.bitwise_not(img_array)
    new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    return new_array.reshape(-1, IMG_SIZE, IMG_SIZE, 1)


model = tf.keras.models.load_model("newest.model")


def validation(filepath, char):
    image_list = os.listdir(filepath)
    counter = 0
    success_count = 0
    index = CATEGORIES.index(char)
    for img in image_list:
        img = os.path.join(filepath, img)
        img_array = prepare(img)
        predict = model.predict([img_array])
        counter += 1
        if (predict[0][index]):
            success_count += 1
    return (success_count, counter)

num_of_images = len(os.listdir(r'C:\Users\Dario\PycharmProjects\CustomOCR\images'))
#print(num_of_images)


for x in range(num_of_images):
    img = prepare("C:\\Users\\Dario\\PycharmProjects\\CustomOCR\\images\\" + str(x) + ".jpg")
    predict = model.predict([img])
    index = np.argmax(predict)
    print(CATEGORIES[index])

"""
img = prepare("C:\\TestniFile\\B.jpg")
predict = model.predict([img])
index = np.argmax(predict)
print(CATEGORIES[index])
"""

"""
success, count = validation("C:\\Users\\Tinac\\Desktop\\Train\\lowercase\\o", "o")

print("success: " + str(success) + "    count:" + str(count) + " percentage: " + str(success / count))
"""