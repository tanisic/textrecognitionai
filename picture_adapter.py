import os
import cv2

ROOTDIR = "C:\\Users\\Tinac\\Desktop\\Train\\lowercase"

CATEGORIES = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
              "v", "w", "x", "y", "z"]
IMG_SIZE = 32

for letter in CATEGORIES:
    letter_dir = os.path.join(ROOTDIR, letter)
    letter_dir_list = os.listdir(letter_dir)
    for img_path in letter_dir_list:
        try:
            img_path = os.path.join(letter_dir, img_path)
            img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
            img = cv2.cv2.bitwise_not(img)
            img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
            cv2.imwrite(img_path, img)
            print("Changed: " + img_path)
        except Exception as e:
            print(e)
