import os
import cv2
from Segmentator import image_prepare as ip

from Segmentator import image_writer as iw
import imutils

class CharacterSegmenter():
    
    def __init__(self,root,minAreaMultiplier,iterations,kernelHeight, kernelWidth):
        self.root=root
        self.min_area_multiplier=minAreaMultiplier/100
        self.iterations=iterations
        self.kernelHeight = kernelHeight
        self.kernelWidth = kernelWidth
    
        
    
    def characters_segment(self):
        line_folders = os.listdir(self.root)
        for (line_number, line_folder) in enumerate(line_folders):
            word_folders = os.listdir(os.path.join(self.root,line_folder))
            for(word_number,word) in enumerate(word_folders):
                image_path = os.path.join(line_folder,word)
                main_image = cv2.imread('../words/'+image_path)
                #image = cv2.resize(main_image,(600,150))
                image = ip.invert(main_image)
                #dilated_image=ip.dilate_character(image)
                dilated_image=ip.dilate_word(image,kernelHeight=self.kernelHeight,kernelWidth=self.kernelWidth)
                cnts = cv2.findContours(dilated_image.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
                cnts = imutils.grab_contours(cnts)
                sorted_ctrs = sorted(cnts, key=lambda ctr: cv2.boundingRect(ctr)[0])
                iw.write_characters(image,sorted_ctrs,line_folder,word,self.min_area_multiplier)

    def character_segment_preview(self,image_path):
        image = cv2.imread(image_path)
        inverted = ip.invert(image)
        dilated_image=ip.dilate_word(inverted,kernelHeight=self.kernelHeight,kernelWidth=self.kernelWidth)
        ctrs,_ = cv2.findContours(dilated_image.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        iw.save_preview_words(image,"characters_preview",ctrs,self.min_area_multiplier)
