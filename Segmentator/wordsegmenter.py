import os
import cv2
from Segmentator import image_prepare as ip
from Segmentator import image_writer as iw


class WordSegmenter():

    def __init__(self, folder_path, minArea_multiplier,iterations,kernelHeight,kernelWidth):
        self.folder = folder_path
        self.minArea_multiplier = minArea_multiplier
        self.iterations = iterations
        self.kernelHeight = kernelHeight
        self.kernelWidth = kernelWidth

    def word_segment(self):
        imgFiles = os.listdir(self.folder)

        for (i, f) in enumerate(imgFiles):
            main_image = cv2.imread('../lines/%s' % f)
            image = ip.invert(main_image)
            height, width = image.shape
            #dilated_image = ip.dilate_without_kernel(image, self.iterations)
            dilated_image= ip.dilate_word(image,self.kernelHeight,self.kernelWidth)
            ctrs, _ = cv2.findContours(dilated_image.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            res = []
            for c in ctrs:
                if cv2.contourArea(c) < self.minArea_multiplier * height * width:
                    continue

                # append bounding box and image of word to result list
                currBox = cv2.boundingRect(c)  # returns (x, y, w, h)
                (x, y, w, h) = currBox
                currImg = main_image[y:y + h, x:x + w]
                res.append((currBox, currImg))

            result = sorted(res, key=lambda entry: entry[0][0])
            iw.write_words(result, f)

    def word_segment_preview(self, image_path):
        image = cv2.imread(image_path)
        inverted = ip.invert(image)
        dilated_image = ip.dilate_word(inverted,self.kernelHeight,self.kernelWidth)
        ctrs, _ = cv2.findContours(dilated_image.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        iw.save_preview_words(image,"words_preview",ctrs,self.minArea_multiplier)
        