import sys
from Segmentator import linesegmenter
from Segmentator import wordsegmenter
from Segmentator import charactersegmenter
import os

def main(image_path,ls_kernelHeight,ls_kernelWidth,ws_kernelHeight,ws_kernelWidth,ws_minAreaMultiplier,cs_kernelHeight,cs_kernelWidth,cs_minAreaMultiplier):

    delete_preview_content("../GUI/preview")
    print(image_path)
    ls = linesegmenter.LineSegmenter(image_path, kernelHeight=ls_kernelHeight, kernelWidth=ls_kernelWidth)
    ls.lines_segment(preview = True)
    ws = wordsegmenter.WordSegmenter(None, minArea_multiplier=ws_minAreaMultiplier,iterations=20,kernelHeight=ws_kernelHeight, kernelWidth=ws_kernelWidth)
    ws.word_segment_preview(image_path)
    cs = charactersegmenter.CharacterSegmenter(None, minAreaMultiplier=cs_minAreaMultiplier, iterations=1, kernelHeight=cs_kernelHeight,
                                               kernelWidth=cs_kernelWidth)
    cs.character_segment_preview(image_path)



def delete_preview_content(folder):
    for filename in os.listdir(folder):
        if filename:
            file_path= os.path.join(folder,filename)
            os.remove(file_path)
