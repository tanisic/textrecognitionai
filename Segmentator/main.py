import sys
from . import linesegmenter, wordsegmenter, charactersegmenter

import os
import tensorflow as tf
import numpy as np
import cv2
import shutil

ROOT_DIR = os.path.abspath("../" + os.curdir)
WORDS_DIR = os.path.join(ROOT_DIR, "words")
LINES_DIR = os.path.join(ROOT_DIR, "lines")
OUTPUT_DIR = os.path.join(ROOT_DIR, "Characters")

CATEGORIES = [
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
    "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]


def prepare(filepath):
    IMG_SIZE = 32
    img_array = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
    new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    return new_array.reshape(-1, IMG_SIZE, IMG_SIZE, 1)


def main(image_path, model_path, ls_kernelHeight, ls_kernelWidth, ws_kernelHeight, ws_kernelWidth, ws_minAreaMultiplier,
         cs_kernelHeight, cs_kernelWidth, cs_minAreaMultiplier):
    delete_folder_content2(WORDS_DIR)
    delete_folder_content2(LINES_DIR)
    delete_folder_content2(OUTPUT_DIR)

    if os.path.exists('../model_output.txt'):
        os.remove('../model_output.txt')
    print("cs kernel height %f"% cs_kernelHeight)
    print("cs kernel width %f" % cs_kernelWidth)
    print("cs min area %f"%cs_minAreaMultiplier)

    ls = linesegmenter.LineSegmenter(image_path, kernelHeight=ls_kernelHeight, kernelWidth=ls_kernelWidth)
    ls.lines_segment(preview=False)
    ws = wordsegmenter.WordSegmenter(LINES_DIR, minArea_multiplier=ws_minAreaMultiplier, iterations=20,
                                     kernelHeight=ws_kernelHeight, kernelWidth=ws_kernelWidth)
    ws.word_segment()
    cs = charactersegmenter.CharacterSegmenter(WORDS_DIR, minAreaMultiplier=cs_minAreaMultiplier, iterations=1,
                                               kernelHeight=cs_kernelHeight,
                                               kernelWidth=cs_kernelWidth)
    cs.characters_segment()
    convert_image_to_plain_text(model=model_path)


def convert_image_to_plain_text(model):
    text = ""
    IMG_SIZE = 32
    model = tf.keras.models.load_model(model)
    for line in os.listdir(OUTPUT_DIR):
        line = os.path.join(OUTPUT_DIR, line)
        for word in os.listdir(line):
            word = os.path.join(line, word)
            for character in os.listdir(word):
                character = os.path.join(word, character)
                img = prepare(character)
                predict = model.predict([img])
                index = np.argmax(predict)
                text += CATEGORIES[index]
            text += " "
        text += "\n"

    file = open(os.path.join(ROOT_DIR, "model_output.txt"), "w+")
    file.write(text)
    file.close()

def delete_folder_content(folder):
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
                print("Deleting: %s"%file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))


def delete_folder_content2(folder):
    list_dir = os.listdir(folder)
    for filename in list_dir:

        file_path = os.path.join(folder, filename)

        if os.path.isfile(file_path) or os.path.islink(file_path):
            print("deleting file:", file_path)

            os.unlink(file_path)

        elif os.path.isdir(file_path):

            print("deleting folder:", file_path)

            shutil.rmtree(file_path)

