import cv2
from Segmentator import image_prepare as ip
from Segmentator import image_writer as iw


class LineSegmenter():

    def __init__(self, image_path, kernelHeight, kernelWidth):
        self.image = cv2.imread(image_path)
        self.kernelHeight = kernelHeight
        self.kernelWidth = kernelWidth

    def lines_segment(self, preview):
        image = ip.invert(self.image)
        dilated_image = ip.dilate_word(image, self.kernelHeight, self.kernelWidth)
        ctrs, _ = cv2.findContours(dilated_image.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        sorted_ctrs, boxes = ip.sort_contours(ctrs, "top-to-bottom")
        if preview == True:
            iw.save_preview_lines(self.image, sorted_ctrs)
        else:
            iw.write_lines(self.image, sorted_ctrs)
