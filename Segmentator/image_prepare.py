import cv2
import numpy as np
THRESHOLD_VALUE = 130
MAX_THRESHOLD_VALUE = 255

def invert(image):
    #grayscale
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)

    #binary
    _,thresh = cv2.threshold(gray,127,255,cv2.THRESH_BINARY_INV)

    return thresh

def dilate(thresh_image,height_multiplier,width_multiplier,iterations=1):
    height,width= thresh_image.shape
    dilationHeight=int(height_multiplier * height)
    dilationWidth=int(width_multiplier * width)
    kernel = np.ones((dilationHeight,dilationWidth), np.uint8)
    img_dilation = cv2.dilate(thresh_image, kernel, iterations=iterations)

    return img_dilation

def dilate_without_kernel(thresh_image,iterations=1):
    img_dilation = cv2.dilate(thresh_image, None, iterations=iterations)

    return img_dilation

def sort_contours(cnts, method="left-to-right"):
    reverse = False
    i = 0
    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1

    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
        key=lambda b:b[1][i], reverse=reverse))

    return (cnts, boundingBoxes)

def dilate_character(thresh_image):
    h, w = thresh_image.shape

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, ksize=(1, 2 * h))
    dilated = cv2.dilate(thresh_image, kernel)
    return dilated

def dilate_character2(image):
    h, w = image.shape
    kernel = np.ones((7,1),np.uint8)
    dilated = cv2.dilate(image, kernel,iterations = 1)
    return dilated

def dilate_word(image,kernelHeight,kernelWidth):
    kernel = np.ones((int(kernelHeight),int(kernelWidth)),np.uint8)
    dilated = cv2.dilate(image, kernel,iterations = 1)
    return dilated



