import os
import cv2
from . import image_prepare as ip


def write_lines(image, sorted_ctrs):
    height, width, _ = image.shape
    for i, ctr in enumerate(sorted_ctrs):
        # Get bounding box
        x, y, w, h = cv2.boundingRect(ctr)
        if w < 0.2 * width:
            continue
        # Getting ROI
        roi = image[y:y + h, x:x + w]

        print('Writing line: ../lines/line' + str(i) + ".png")
        cv2.imwrite("../lines/line" + str(i) + ".png", roi)

def save_preview_lines(image,sorted_ctrs):
    height, width, _ = image.shape
    for i, ctr in enumerate(sorted_ctrs):
        # Get bounding box
        x, y, w, h = cv2.boundingRect(ctr)
        # Getting ROI
        roi = image[y:y + h, x:x + w]
        cv2.rectangle(image,(x,y),( x + w, y + h ),(44,255,58),2)
    cv2.imwrite("../GUI/preview/line_preview.png",image)

def save_preview_words(image,name,ctrs,minArea):
    height, width, _ = image.shape
    print("height %d" % height)
    print("width %d" % width)
    for c in ctrs:
        if cv2.contourArea(c) < minArea * height * width:
            continue

        # append bounding box and image of word to result list
        currBox = cv2.boundingRect(c)  # returns (x, y, w, h)
        (x, y, w, h) = currBox
        if name == "characters_preview": 
            cv2.rectangle(image,(x,y),( x + w, y + h ),(90,0,255),2)
        else: 
            cv2.rectangle(image,(x,y),( x + w, y + h ),(232,101,68),2)
    cv2.imwrite("../GUI/preview/%s.png"%name,image)



def write_words(result, f):
    if not os.path.exists('../words/%s' % f):
        os.mkdir('../words/%s' % f)

    for (j, w) in enumerate(result):
        (wordBox, wordImg) = w
        (x, y, w, h) = wordBox
        print('Writing word: ../words/%s/word%d.png' % (f, j))
        cv2.imwrite('../words/%s/word%d.png' % (f, j), wordImg)  # save word


def write_characters(image, cnts, line_folder, word_folder, MINAREA_MULTIPLIER):
    height, width = image.shape
    print("height %d"%height)
    print("width %d" % width)
    if not os.path.exists('../Characters/%s' % line_folder):
        os.mkdir('../Characters/%s' % line_folder)

    if not os.path.exists('../Characters/%s/%s' % (line_folder, word_folder)):
        os.mkdir('../Characters/%s/%s' % (line_folder, word_folder))
    IMG_SIZE = 50
    BORDER_OFFSET = 10
    counter = 0
    for c in cnts:
        if cv2.contourArea(c) < MINAREA_MULTIPLIER * height * width * 100:
            continue

        (x, y, w, h) = cv2.boundingRect(c)
        roi = image[y:y + h, x:x + w]
        (tH, tW) = roi.shape
        (tH, tW) = roi.shape
        dX = int(max(0, 32 - tW) / 2.0) + BORDER_OFFSET
        dY = int(max(0, 32 - tH) / 2.0) + BORDER_OFFSET
        # pad the image and force 32x32 dimensions
        padded = cv2.copyMakeBorder(roi, top=dY, bottom=dY, left=dX, right=dX, borderType=cv2.BORDER_CONSTANT,
                                    value=(0, 0, 0))
        padded = ip.dilate(padded, 0.02, 0.02, 3)
        _, padded = cv2.threshold(padded, 127, 255, cv2.THRESH_BINARY)
        #padded = cv2.resize(padded, (32, 32))

        image_name = str(counter) + ".png"
        print('Writing character: ../Characters/%s/%s/%s' % (line_folder, word_folder, image_name))
        cv2.imwrite('../Characters/%s/%s/%s' % (line_folder, word_folder, image_name), padded)
        counter += 1
