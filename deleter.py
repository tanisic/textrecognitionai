import os

CATEGORIES = ["#", "$", "&", "@", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
              "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
              "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]


def delete_files(root_directory, imgs_per_folder):
    for category in CATEGORIES:
        dir = os.path.join(root_directory, category)
        dir_list = os.listdir(dir)
        count = 0
        for img in dir_list:
            img = os.path.join(dir,img)
            if count >= imgs_per_folder:
                os.remove(img)
                print("removed: " + img)
                count += 1
            else:
                count += 1


delete_files("C:\\Users\\Tinac\\Desktop\\Validation", 1200)
