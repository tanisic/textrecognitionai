# Configure virtual environment
- In project root execute `` python -m venv venv``
- Activate virtual environment `` source venv/Scripts/activate ``
- Install requiered libraries `` pip install -r requierements.txt``

# Updating local repository
- `` git remote add upstream https://gitlab.com/tanisic/textrecognitionai``
- `` git fetch upstream master``
- `` git merge upstream/master``

# How to use validation

- Validaciju šaljemo u train.py pozivom metode validation(filepath, index)
- filepath je putanja do foldera u kojemu se nalazi znak, a char je znak koji se treba nalaziti u tom direktoriju ,
npr.
  success, count = validation("C:\\Users\\Tinac\\Desktop\\Validation\\A", "A")
  
# How to use deleter.py
- Služi za brisanje previše podataka iz training direktorija
- Unesete root direktorij training podataka i broj slika kojih zelite imati po kategoriji
- funkcija sama briše višak fotografija (puno brze nego rucno brisanje)
-npr. delete_files("C:\\Users\\Tinac\\Desktop\\Validation", 1200)

# Link na datasetove za treniranje i validaciju
- https://www.kaggle.com/vaibhao/handwritten-characters

# How to use segmentator (WIP)
- Put image you want to segment into images/
- in src/main.py choose that path as an input image
- adjust segmentation settings in main.py
- run main.py 
- segmented lines are in lines/
- segmented words are in words/
- segmented characters are in Characters/

